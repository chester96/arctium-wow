﻿/*
 * Copyright (C) 2012-2014 Arctium Emulation <http://arctium.org>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Framework.Constants.Misc;
using Framework.Database;
using Framework.Database.Auth.Entities;
using Framework.Logging;
using Framework.Misc;

namespace AuthServer.Managers
{
    class ModuleManager : Singleton<ModuleManager>
    {
        public bool IsInitialized { get; private set; }
        public readonly List<Module> ModuleList;
        public readonly List<Module> Module64List;
        public readonly List<Module> ModuleMacList;

        ModuleManager()
        {
            ModuleList = new List<Module>();
            Module64List = new List<Module>();
            ModuleMacList = new List<Module>();
            IsInitialized = false;

            UpdateModules();
        }

        public void UpdateModules()
        {
            Log.Message(LogType.Debug, "Loading modules...");

            var modules = DB.Auth.Modules.Select(m => m);

            foreach (var CurrentModule in modules)
            {
                bool l_Result = false;

                switch (CurrentModule.System)
                {
                    case "Win":
                        l_Result = AddModule(CurrentModule, ModuleList);
                        break;

                    case "Wn64":
                        l_Result = AddModule(CurrentModule, Module64List);
                        break;

                    case "Mc64":
                        l_Result = AddModule(CurrentModule, ModuleMacList);
                        break;

                    default:
                        Log.Message(LogType.Error, "System {0} not handled for {1} module '{2}({3})'", CurrentModule.System, CurrentModule.Type, CurrentModule.Hash, CurrentModule.Name);
                        break;
                }


                if (l_Result)
                    Log.Message(LogType.Debug, "New {0} {1} module '{2}({3})' loaded", CurrentModule.System, CurrentModule.Type, CurrentModule.Hash, CurrentModule.Name);
            }

            Log.Message(LogType.Debug, "Successfully loaded {0} modules", ModuleList.Count + + Module64List.Count + ModuleMacList.Count);

            IsInitialized = true;
        }

        bool AddModule(Module module, IList list)
        {
            if (!list.Contains(module))
                return list.Add(module) != -1;

            return false;
        }
    }
}
