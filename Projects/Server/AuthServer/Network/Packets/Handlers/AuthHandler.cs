﻿/*
 * Copyright (C) 2012-2014 Arctium Emulation <http://arctium.org>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using AuthServer.Attributes;
using AuthServer.Constants.Authentication;
using AuthServer.Constants.Net;
using AuthServer.Managers;
using Framework.Constants.Misc;
using Framework.Cryptography.BNet;
using Framework.Logging;
using Framework.Misc;
using Framework.Network.Packets;

namespace AuthServer.Network.Packets.Handlers
{
    class AuthHandler
    {
        public static void SendProofRequest(AuthSession session)
        {
            var proofRequest = new AuthPacket(AuthServerMessage.ProofRequest);

            proofRequest.Write(2, 3);

            session.Modules.ForEach(module =>
            {
                // Only auth modules are supported here.
                if (module.Type == "auth")
                {
                    switch (module.Name)
                    {
                        case "Password":
                        case "Thumbprint":
                            proofRequest.WriteFourCC(module.Type);                         ///< Module type ("auth" in this context)
                            proofRequest.WriteFourCC("\0\0" + session.Account.Region);     ///< Module region
                            proofRequest.Write(module.Hash.ToByteArray());                 ///< Module hash (used as ID in client)
                            proofRequest.Write(module.Size, 10);                           ///< Module data size

                            break;

                        default:
                            break;
                    }

                    switch (module.Name)
                    {
                        case "Thumbprint":
                            proofRequest.Write(module.Data.ToByteArray());
                            break;

                        case "Password":
                            session.SecureRemotePassword = new SRP6a(session.Account.Salt, session.Account.Email, session.Account.PasswordVerifier);
                            session.SecureRemotePassword.CalculateB();

                            // Flush & write the state id
                            proofRequest.Flush();
                            proofRequest.Write(AuthProofState.Request, 8);                  ///< Proof state

                            proofRequest.Write(session.SecureRemotePassword.I);
                            proofRequest.Write(session.SecureRemotePassword.S);
                            proofRequest.Write(session.SecureRemotePassword.B);
                            proofRequest.Write(session.SecureRemotePassword.S2);
                            break;

                        default:
                            break;
                    }
                }
            });

            session.Send(proofRequest);
        }

        [AuthMessage(AuthClientMessage.ProofResponse, AuthChannel.BattleNet)]
        public static void OnProofResponse(AuthPacket packet, AuthSession session)
        {
            var moduleCount = packet.Read<byte>(3);

            for (int i = 0; i < moduleCount; i++)
            {
                var dataSize = packet.Read<int>(10);
                var stateId = packet.Read(1)[0];

                switch ((AuthProofState)stateId)
                {
                    case AuthProofState.AuthSuccess:
                        SendAuthComplete(AuthResult.GlobalSuccess, session);
                        break;
                    case AuthProofState.RequestResult:
                        // Wrong password module data size
                        if (dataSize != 0x121)
                            return;

                        var a = packet.Read(0x80);
                        var m1 = packet.Read(0x20);
                        var clientChallenge = packet.Read(0x80);

                        session.SecureRemotePassword.CalculateU(a);
                        session.SecureRemotePassword.CalculateClientM(a);

                        if (session.SecureRemotePassword.ClientM.Compare(m1))
                        {
                            session.SecureRemotePassword.CalculateServerM(m1);

                            SendProofVerification(session, clientChallenge);
                        }
                        else
                            SendAuthComplete(AuthResult.BadLoginInformation, session);

                        break;
                    default:
                        break;
                }
            }
        }

        public static void SendProofVerification(AuthSession session, byte[] clientChallenge)
        {
            var proofVerification = new AuthPacket(AuthServerMessage.ProofRequest);

            proofVerification.Write(2, 3);

            session.Modules.ForEach(module =>
            {
                // Only auth modules are supported here.
                if (module.Type == "auth")
                {
                    switch (module.Name)
                    {
                        case "Password":
                        case "RiskFingerprint":
                            proofVerification.WriteFourCC(module.Type);                         ///< Module type ("auth" in this context)
                            proofVerification.WriteFourCC("\0\0" + session.Account.Region);     ///< Module region
                            proofVerification.Write(module.Hash.ToByteArray());                 ///< Module hash (used as ID in client)
                            proofVerification.Write(module.Size, 10);                           ///< Module data size

                            break;

                        default:
                            break;
                    }

                    switch (module.Name)
                    {
                        case "Password":
                            proofVerification.Flush();
                            proofVerification.Write(AuthProofState.VerificationRequest, 8);     ///< Proof state

                            proofVerification.Write(session.SecureRemotePassword.ServerM);
                            proofVerification.Write(session.SecureRemotePassword.S2);

                            break;

                        default:
                            break;
                    }
                }
            });

            session.Send(proofVerification);
        }

        public static void SendAuthComplete(AuthResult result, AuthSession session)
        {
            var complete = new AuthPacket(AuthServerMessage.Complete);

            /// If the auth failed
            if (complete.Write(result != AuthResult.GlobalSuccess, 1))
            {
                complete.Write(false, 1);       // false - disable optional modules
                complete.Write(1, 2);           // 1 - enable AuthResults
                complete.Write(result, 16);     // AuthResults (Error codes)
                complete.Write(0x80000000, 32); // Unknown
            }
            else
            {
                var hasOptionalData = true;
                var hasConnectionInfo = true;

                complete.Write(0, 3);
                complete.Write(0x80005000, 32); // Ping request, ~10 secs

                if (complete.Write(hasOptionalData, 1))
                {
                    if (complete.Write(hasConnectionInfo, 1))   ///< Battlenet::Regulator::LeakyBucketParams
                    {
                        complete.Write(25000000, 32);           ///< Threshold
                        complete.Write(1000, 32);               ///< Rate
                    }
                }

                complete.Write(false, 1);

                complete.WriteString("", 8, false); // FirstName not implemented
                complete.WriteString("", 8, false); // LastName not implemented

                complete.Write(session.Account.Id, 32);

                complete.Write(0, 8);
                complete.Write(0, 64);
                complete.Write(0, 8);

                complete.WriteString(session.Account.Email, 5, false, -1);

                complete.Write(0, 64);
                complete.Write(0, 32);

                complete.Write(0, 8);
            }

            session.Send(complete);
        }
    }
}
